#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 19 14:32:40 2022

@author: ddutartr
"""
import torch
import torch.nn as nn
from torch import optim
from torch.autograd import Variable
import os
import numpy as np

from model import Generator, Encoder, Discriminator
from utils import weights_init_normal

from prepro import prepro_fluo,patch_decomposition,patch_feed,patch_recomposition

class EGBADtrain():
    def __init__(self, args, data, device):
        self.args = args
        self.train_data = data
        self.device = device
        self.build_models()
        self.save_images = True
        
        
        
    def train(self):
        if self.args.load:
            self.load_models()
        
        alpha = 0.9
        netE , netG, netD = self.E,self.G,self.D
        lr_ge = self.args.lr
        lr_d = self.args.lr/4
        criterion = torch.nn.BCELoss()
        criterion = nn.BCEWithLogitsLoss(reduction="mean")
        criterion = torch.nn.BCELoss()
        optimizerG = optim.Adam(netG.parameters(), lr=lr_ge, betas=(0.5, 0.999))
        optimizerE = optim.Adam(netE.parameters(), lr=lr_ge, betas=(0.5, 0.999))

        optimizerD = optim.Adam(netD.parameters(), lr = lr_d , betas = (0.5,0.999))
        
        for epoch in range(self.args.epoch):
            GE_losses = []
            D_losses = []
            ge_loss = 0
            d_loss = 0
            for i,xy in enumerate(self.train_data, 0):
                x= xy[0]
                y_true = Variable(torch.ones((x.size(0), 1)).to(self.device))
                y_fake = Variable(torch.zeros((x.size(0), 1)).to(self.device))
                images= x

                images = images.to(device)
                
                images= x
                if self.args.is_patch:
                    patches = patch_decomposition(images , size = 512 ,stride = 512)
                    patches_in = patch_feed(patches , batch_size = egbad.args.batch_size , size = 512 , stride = 512)
                    images = patches_in.to(device)
                else:
                    images = images.to(device)
                
                y_true = Variable(torch.ones((images.size(0), 1)).to(egbad.device))
                y_fake = Variable(torch.zeros((images.size(0), 1)).to(egbad.device))
                
                '''
                Learning Discriminator
                '''
                netD.zero_grad()
                # True computation
                z_true = netE.forward(images) #z_gen
                
                x_true = images.float()
                # Add noise
                noise_true = Variable(torch.Tensor(images.size()).normal_(0, 0.1 * (self.args.epoch - epoch) / self.args.epoch),
                               requires_grad=False).to(self.device)
                
                output_true,_  = netD.forward(x_true + noise_true, z_true)
                
                # Fake computation
                noise_fake = Variable(torch.Tensor(images.size()).normal_(0, 0.1 * (self.args.epoch - epoch) / self.args.epoch),
                               requires_grad=False).to(self.device)
                # Add noise
                z_fake = Variable(torch.randn((images.size(0), self.args.latent_dim, 1, 1)).to(device),
                                  requires_grad=False)      
                x_fake = netG.forward(z_fake) #x_gen         
                output_fake,_  = netD.forward(x_fake + noise_fake, z_fake)
      
                #Loss function
                loss_D = criterion(output_true, y_true.view(-1)) + criterion(output_fake, y_fake.view(-1))
                loss_D.backward(retain_graph=True)
                optimizerD.step()
                '''
                Learning Generator
                '''
                netG.zero_grad()
                z_fake = Variable(torch.randn((images.size(0), self.args.latent_dim, 1, 1)).to(device),
                                  requires_grad=False)    
                noise_fake = Variable(torch.Tensor(images.size()).normal_(0, 0.1 * (self.args.epoch - epoch) / self.args.epoch),
                               requires_grad=False).to(self.device)
                x_fake = netG.forward(z_fake) #x_gen         
                output_fake,_  = netD.forward(x_fake + noise_fake, z_fake)
                loss_G = criterion(output_fake,y_true.view(-1))
                
                loss_G.backward()
                optimizerG.step()
                '''
                Learning Encorder
                '''          
                netE.zero_grad()
                z_true = netE.forward(images) #z_gen
                noise_true = Variable(torch.Tensor(images.size()).normal_(0, 0.1 * (self.args.epoch - epoch) / self.args.epoch),
                               requires_grad=False).to(self.device)
                output_true,_  = netD.forward(x_true + noise_true, z_true)
                loss_E = criterion(output_true, y_fake.view(-1))

              # optimizerG.zero_grad()
                loss_E.backward()

                optimizerE.step()
                
                if i %10 ==0 :
                    print("{}th iteration gen_loss: {} dis_loss: {} enc_loss:{}".format(i,loss_G.data,loss_D.data,loss_E.data))
            print(epoch)
    def build_models(self):
        self.G = Generator(self.args.latent_dim, self.args.dim_color, self.args.n_feat, self.args.img_size).to(self.device)
        self.E = Encoder(self.args.latent_dim, self.args.dim_color, self.args.n_feat, self.args.img_size).to(self.device)
        self.D = Discriminator(self.args.latent_dim, self.args.img_size, self.args.dim_color, self.args.n_feat).to(self.device)
        self.G.apply(weights_init_normal)
        self.E.apply(weights_init_normal)
        self.D.apply(weights_init_normal)
        
    def save_models(self):
        
        os.makedirs('./saved_models', exist_ok=True)
        state_dict_G = self.G.state_dict()
        state_dict_E = self.E.state_dict()
        state_dict_D = self.D.state_dict()
        torch.save({'Generator': state_dict_G,
                    'Encoder': state_dict_E,
                    'Discriminator': state_dict_D},'saved_models/model_parameters.pth')
        
    def load_models(self):
        if self.device.type == 'cpu':
            state_dict = torch.load( 'saved_models/model_parameters.pth',map_location=torch.device('cpu'))
        else:
            state_dict = torch.load( 'saved_models/model_parameters.pth')
        
        self.G.load_state_dict(state_dict['Generator'])
        self.E.load_state_dict(state_dict['Encoder'])
        self.D.load_state_dict(state_dict['Discriminator'])           



   
class Args:
    # Recommanded parameters for Fluo images
    epoch=1000
    lr=5e-5
    latent_dim=512
    batch_size=2
    load=False
    img_size = 512
    dim_color = 1
    n_feat = 100
    is_patch = False

if __name__ == '__main__':
    device = torch.device("cuda:0" if (torch.cuda.is_available()) else "cpu")

    args = Args()
    data_root = '/home/ddutartr/Projet/MONC/DATA/Fluorescence/HEp-2/'
    if args.is_patch:
        dataloader_train = prepro_fluo(data_dir = data_root , img_size = (1024,1536) , batch_size = args.batch_size)
    else:
        dataloader_train = prepro_fluo(data_dir = data_root , img_size = (args.img_size,args.img_size) , batch_size = args.batch_size)
    egbad = EGBADtrain(args, dataloader_train, device)
    torch.autograd.set_detect_anomaly(True)
    egbad.train()
    egbad.save_models()
