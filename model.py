#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 11:42:38 2021

@author: ddutartr
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchsummary import summary
import math


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)

class Generator(nn.Module):
    def __init__(self,
                 dimLatentVector,
                 dim_color,
                 n_feat = 64,
                 img_size = 64,
                 bias = True,
                 functionActivation=nn.Tanh()):
        super(Generator, self).__init__()
        depthModel = int(math.log(img_size)/math.log(2) - 3)
        depthModel = int(math.log(img_size)/math.log(4) - 1)
        self.depthModel = depthModel
        self.refDim = img_size
        self.bias = bias
        self.functionActivation = functionActivation
        self.dimLatentVector = dimLatentVector
        if bias:
            self.output_bias = nn.Parameter(torch.zeros(1, img_size, img_size), requires_grad=True)
        
        def block(in_feat, out_feat, kernel , stride, padding , bias = False ,normalize=True):
            layers = [nn.ConvTranspose2d(in_feat, out_feat , kernel , stride , padding , bias)]
            if normalize:
                layers.append(nn.BatchNorm2d(out_feat))
            layers.append(nn.ReLU(inplace=False))
            return layers
        # Initialize layer
        currDepth = int(n_feat * (2**depthModel))
     

        sequence  = []
        
        # input is Z, going into a convolution
        sequence = sequence + block(in_feat = dimLatentVector , 
                                      out_feat = currDepth , 
                                      kernel = 4 , 
                                      stride = 1 , 
                                      padding = 0 , 
                                      bias = False , 
                                      normalize = True)
                       

        for i in range(depthModel):

            nextDepth = int(currDepth / 2)
            sequence = sequence + block(in_feat = currDepth , 
                                                  out_feat = nextDepth , 
                                                  kernel = 8 , 
                                                  stride = 4 , 
                                                  padding = 2 , 
                                                  bias = False , 
                                                  normalize = True)
                           

            # state size. (currDepth) x 2**(i+1) x 2**(i+1)
            currDepth = nextDepth

        sequence = sequence + [nn.ConvTranspose2d(n_feat, dim_color, 4, 2, 1, bias=False )]

        self.model = nn.Sequential( *sequence )
        self.model.apply(weights_init)


    def forward(self, in_noise):


        x = self.model(in_noise)
        if self.bias:
            output = self.functionActivation( x + self.output_bias)
        else:
            output = self.functionActivation( x )
        return output

    
class Discriminator(nn.Module):
    def __init__(self,
                 dimLatentVector,
                 img_size,
                 dim_color,
                 n_feat,
                 dropout = 0.25,
                 sizeJointLayer = 512
                 ):
        super(Discriminator, self).__init__()
        self.sizeJointLayer = sizeJointLayer
        self.output_size = 1
        self.dropout =  dropout
        self.dimLatentVector = dimLatentVector
        self.img_size = img_size
        self.dim_color = dim_color
        self.n_feat = n_feat
        self.depth = int(math.log(self.img_size)/math.log(2) -2)
        def block(in_feat, out_feat, kernel , stride, padding , bias = False , normalize = True, p_drop = 0.5):
            layers = [nn.Conv2d(in_feat, out_feat , kernel , stride , padding , bias = bias)]
            if normalize:
                layers.append(nn.BatchNorm2d(out_feat))
            layers.append(nn.LeakyReLU(0.1, inplace=False))
            layers.append(nn.Dropout(p=p_drop))
            return layers
        def seq_inf_x( n_feat , img_size , dim_color , dropout , sizeJointLayer):
            currDepth = n_feat
            depthModel = int(math.log(img_size)/math.log(4) - 1)
            sequence  = []

            # input is (nc) x 2**(depthModel + 3) x 2**(depthModel + 3)
            sequence = sequence + block(in_feat = dim_color , 
                          out_feat = currDepth , 
                          kernel = 4 , 
                          stride = 2 , 
                          padding = 1 , 
                          bias = False , 
                          normalize = False,
                          p_drop = 0.5)
        
            for i in range(depthModel):

                nextDepth = int(currDepth * 2)
                sequence = sequence + block(in_feat = currDepth , 
                              out_feat = nextDepth , 
                              kernel = 8 , 
                              stride = 4 , 
                              padding = 3 , 
                              bias = False , 
                              normalize = True ,
                              p_drop = dropout)

                currDepth = nextDepth

            sequence = sequence + block(in_feat = currDepth , 
                              out_feat = sizeJointLayer , 
                              kernel = 4 , 
                              stride = 1 , 
                              padding = 0 , 
                              bias = False , 
                              normalize = True ,
                              p_drop = dropout)

            infx = nn.Sequential( *sequence )
            infx.apply(weights_init)
            return infx
        
        def seq_inf_z( dimLatentVector , sizeJointLayer , dropout):
            sequence  = []
            sequence = sequence + block(in_feat = dimLatentVector , 
                          out_feat = sizeJointLayer , 
                          kernel = 1 , 
                          stride = 1 , 
                          padding = 0 , 
                          bias = False , 
                          normalize = False,
                          p_drop = dropout)
            
            sequence = sequence + block(in_feat = sizeJointLayer , 
                          out_feat = sizeJointLayer , 
                          kernel = 1 , 
                          stride = 1 , 
                          padding = 0 , 
                          bias = False , 
                          normalize = False,
                          p_drop = dropout)
            infz = nn.Sequential( *sequence )
            infz.apply(weights_init)
            return infz
        def seq_inf_xz(sizeJointLayer,dropout):
            sequence = []
            sequence = sequence + block(in_feat = sizeJointLayer*2 , 
                          out_feat = sizeJointLayer*2 , 
                          kernel = 1 , 
                          stride = 1 , 
                          padding = 0 , 
                          bias = False , 
                          normalize = False,
                          p_drop = dropout)
            sequence = sequence + block(in_feat = sizeJointLayer*2 , 
                          out_feat = sizeJointLayer*2 , 
                          kernel = 1 , 
                          stride = 1 , 
                          padding = 0 , 
                          bias = False , 
                          normalize = False,
                          p_drop = dropout)
            infxz = nn.Sequential( *sequence )
            infxz.apply(weights_init)
            return infxz
        
        self.infx = seq_inf_x(self.n_feat, self.img_size, self.dim_color, self.dropout , self.sizeJointLayer)
        self.infz = seq_inf_z( self.dimLatentVector , self.sizeJointLayer , self.dropout)
        self.infxz = seq_inf_xz (self.sizeJointLayer,self.dropout)
        self.final = nn.Conv2d(self.sizeJointLayer*2, self.output_size, 1, stride=1, bias=False)
        self.final.apply(weights_init)
    def forward(self, x , z):
        output_x = self.infx(x)
        output_z = self.infz(z)
        xz = torch.cat((output_x,output_z),dim=1)
        out = self.infxz(xz)
        #return F.sigmoid(out),out
        return torch.sigmoid(self.final(out)).view(-1),out

class Encoder(nn.Module):
    def __init__(self,
                 dimLatentVector,
                 dim_color,
                 n_feat = 64,
                 img_size = 64,
                 reparametrization_trick = False):
        super(Encoder, self).__init__()
        def block(in_feat, out_feat, kernel , stride, padding , bias = False , normalize = True, p_drop = 0.5):
            layers = [nn.Conv2d(in_feat, out_feat , kernel , stride , padding , bias = bias)]
            if normalize:
                layers.append(nn.BatchNorm2d(out_feat))
            layers.append(nn.LeakyReLU(0.1, inplace=False))
            layers.append(nn.Dropout(p=p_drop))
            return layers
        currDepth = n_feat
        depthModel = int(math.log(img_size)/math.log(2) - 3)
        depthModel = int(math.log(img_size)/math.log(4) - 1)
        sequence  = []
        self.depthModel = depthModel
        self.refDim = img_size
        self.reparametrization = reparametrization_trick
        self.sizeDecisionLayer = dimLatentVector*2 if reparametrization_trick else dimLatentVector
        self.dimLatentVector = dimLatentVector
        self.dropout = 0.25
        # input is (nc) x 2**(depthModel + 3) x 2**(depthModel + 3)
        sequence = sequence + block(in_feat = dim_color , 
                          out_feat = currDepth , 
                          kernel = 4 , 
                          stride = 2 , 
                          padding = 1 , 
                          bias = False , 
                          normalize = False,
                          p_drop = 0.5)
        for i in range(depthModel):

            nextDepth = int(currDepth * 2)
            sequence = sequence + block(in_feat = currDepth , 
                              out_feat = nextDepth , 
                              kernel = 8 , 
                              stride = 4 , 
                              padding = 3 , 
                              bias = False , 
                              normalize = True ,
                              p_drop = self.dropout)

            currDepth = nextDepth

        sequence = sequence + [nn.Conv2d(currDepth, self.sizeDecisionLayer, 4, 1, 0, bias=False )]
        
        self.model = nn.Sequential( *sequence )
        self.model.apply(weights_init)
    def reparameterize(self , z):
        # Reparametriztion trick
        mu , log_sigma = z[: , :self.dimLatentVector] , z[: , self.dimLatentVector:]
        sigma = torch.exp(log_sigma)
        eps = torch.randn_like(sigma)
        return mu + eps*sigma
    def forward(self, in_img):


        x = self.model(in_img)
        if self.reparametrization:
            output = self.reparameterize( x )
        else:
            output = x
        return output
    
    


if __name__ == '__main__':
    latent_dim = 512
    img_size = 512
    G = Generator(dimLatentVector = latent_dim,
              dim_color = 1,
              n_feat = 100,
              img_size = img_size,
              bias = True)
    print('Generator shape \n')
    summary(G,(latent_dim,1,1))
    D = Discriminator(dimLatentVector = latent_dim,
                 img_size = img_size,
                 dim_color = 1,
                 n_feat = 64)
    print('Discriminator shape \n')
    summary(D, [(1 , img_size , img_size),(latent_dim,1,1)])
    E = Encoder(dimLatentVector= latent_dim,
                 dim_color = 1,
                 n_feat = 100,
                 img_size = img_size)
    print('Encoder shape \n')
    summary(E,(1,img_size,img_size))


