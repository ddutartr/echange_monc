#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:46:44 2022

@author: ddutartr
"""
import torch
import torch.nn as nn
from torch import optim
from torch.autograd import Variable
import torch.nn.functional as F
import torchvision.utils as vutils
import os
import numpy as np

from model import Generator, Encoder, Discriminator
from utils import weights_init_normal
from torchsummary import summary

from prepro import prepro_fluo
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA, TruncatedSVD

class EGBADtest():
    def __init__(self, args, data, device):
        self.args = args
        self.test_data = data
        self.device = device
        self.build_models()
        self.save_images = True
        self.load_models()
        self.G.eval()
        self.E.eval()
        self.D.eval()
    def build_models(self):
        self.G = Generator(self.args.latent_dim, self.args.dim_color, self.args.n_feat, self.args.img_size).to(self.device)
        self.E = Encoder(self.args.latent_dim, self.args.dim_color, self.args.n_feat, self.args.img_size).to(self.device)
        self.D = Discriminator(self.args.latent_dim, self.args.img_size, self.args.dim_color, self.args.n_feat).to(self.device)
        self.G.apply(weights_init_normal)
        self.E.apply(weights_init_normal)
        self.D.apply(weights_init_normal)
        
    def save_models(self):
        
        os.makedirs('./saved_models', exist_ok=True)
        state_dict_G = self.G.state_dict()
        state_dict_E = self.E.state_dict()
        state_dict_D = self.D.state_dict()
        torch.save({'Generator': state_dict_G,
                    'Encoder': state_dict_E,
                    'Discriminator': state_dict_D},'saved_models/model_parameters.pth')
        
    def load_models(self):
        state_dict = torch.load( 'saved_models/' + self.args.model_name,map_location=torch.device('cpu'))
        
        self.G.load_state_dict(state_dict['Generator'])
        self.E.load_state_dict(state_dict['Encoder'])
        self.D.load_state_dict(state_dict['Discriminator'])
        self.G.eval()
        self.E.eval()
        self.D.eval()
    
    def do_tsne_plot(self):
        if self.args.is_patch:
            print("Not implemented yet")
            # Almost the same that without patch execpt :
            # Need to feed the encore with decompose images in patch
            # Need to reconstruct the Z_gen with the correct format
        else:
            X = []
            Y = []
            for i_batch, (data,target) in enumerate(self.test_data):
                print(i_batch)
                imgs = data.to(device)

                Z_gen = self.E.forward(imgs)
                #G_reco = self.G.forward(Z_gen)
                Y.extend(target.numpy())
                X.extend(np.squeeze(Z_gen.detach().numpy()))
            X1 = np.array(X)
            X_r = TSNE(n_components=2, random_state = 1234).fit_transform(X1)
            Y1 = np.array(Y)
            fig, ax = plt.subplots()
            scatter=ax.scatter(X_r[:,0],X_r[:,1],s=5,c=Y1)
            plt.title('TSNE')
            plt.xlabel('First Dimension')
            plt.ylabel('Second Dimension')
            legend1 = ax.legend(*scatter.legend_elements(num=4),
                    loc="upper left", title="Folder")
            ax.add_artist(legend1)
            plt.savefig(self.model_type+'_TSNE.png')
            X_r = PCA(n_components=2, random_state=1234).fit_transform(X1)
            fig, ax = plt.subplots()
            scatter=ax.scatter(X_r[:,0],X_r[:,1],s=5,c=Y1)
            plt.title('PCA')
            plt.xlabel('First Dimension')
            plt.ylabel('Second Dimension')
            legend1 = ax.legend(*scatter.legend_elements(num=4),
                    loc="upper left", title="Folder")
            ax.add_artist(legend1)
            plt.savefig(self.model_type+'_PCA.png')
    
            X_r = TruncatedSVD(n_components=2, random_state=1234).fit_transform(X1)
            fig, ax = plt.subplots()
            scatter=ax.scatter(X_r[:,0],X_r[:,1],s=5,c=Y1)
            plt.title('Truncated SVD')
            plt.xlabel('First Dimension')
            plt.ylabel('Second Dimension')
            legend1 = ax.legend(*scatter.legend_elements(num=4),
                    loc="upper left", title="Folder")
            ax.add_artist(legend1)
            plt.savefig(self.model_type+'_TSVD.png')
    
    def do_anomaly_plot(self):
        alpha = 0.9
        D_loss = []
        EG_loss = []
        Anomaly_Score_test =[]
        label_tot = []
        for i, data in enumerate(self.test_data) :
            print(i)
            images, labels = data
            
            images = images.to(device)

            Z_gen = self.E.forward(images)
            G_reco = self.G.forward(Z_gen)

            #let's recover the las layer and the output of the discrimintor
            D_E , D_E_F_L = self.D(images,Z_gen)
            D_G , D_G_F_L = self.D(G_reco,Z_gen)

            #reconstruction loss 
            loss_R = torch.norm(images - G_reco, p = 1,dim = (2,3))
            loss_D = torch.norm(D_E_F_L-D_G_F_L, p =1 , dim = 1)
            #Score 

            A_x = alpha*loss_D .squeeze()+ (1-alpha)*loss_R.squeeze()



            Anomaly_Score_test.extend(A_x.detach().numpy())
            label_tot.extend(labels.detach().numpy())


        ano = Anomaly_Score_test
        labe = label_tot
        data = np.array([ano,labe]).T
        df =pd.DataFrame(data,columns=['score','label'])

        ax = sns.boxplot(x="label", y="score", data=df)
        ax.set_ylabel("Anomaly Score")
        ax.set_xlabel("Disease")
        plt.title( 'Anomaly Score per sub-category' )
        plt.show()
        plt.savefig('Boxplot.png')
        plt.close()
        
        n = 10
        plt.figure(figsize=(16,4.5))
        rec_imgs = G_reco
        for i in range(n):
            img = images[i,0,:,:]
            ax = plt.subplot(2,n,i+1)
            plt.imshow(img.numpy(), cmap='gist_gray')
       
            rec_img = rec_imgs[i,0,:,:].detach()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)  
            if i == n//2:
                ax.set_title('Original images')
            ax = plt.subplot(2, n, i + 1 + n)
            plt.imshow(rec_img.squeeze().numpy(), cmap='gist_gray')  
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)  
            if i == n//2:
                ax.set_title('Reconstructed images')        
        plt.savefig('Reconstruct.png')
        plt.close()
        
 
        

class Args:
    # Recommanded parameters for Fluo images
    epoch=1000
    lr=5e-5
    latent_dim=512
    batch_size=2
    load=False
    img_size = 512
    dim_color = 1
    n_feat = 100
    is_patch = False
    model_name='model_parameters.pth'
    model_type = 'EGBAD'

if __name__ == '__main__':
    device = torch.device("cuda:0" if (torch.cuda.is_available()) else "cpu")

    args = Args()
    data_root = '/home/ddutartr/Projet/MONC/DATA/Fluorescence/HEp-2/'
    if args.is_patch:
        data_test = prepro_fluo(data_dir = data_root , img_size = (1024,1536) , batch_size = args.batch_size)
    else:
        data_test = prepro_fluo(data_dir = data_root , img_size = (args.img_size,args.img_size) , batch_size = args.batch_size)
    
    egbad = EGBADtest(args, data_test, device)
    egbad.do_anomaly_plot() # More use for Xray 
    egbad.do_tsne_plot() # More use for Fluo

