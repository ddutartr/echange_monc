#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 14:17:43 2022

@author: ddutartr
"""
import os
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
import torchvision.datasets as dset
import shutil

#data_root = '/home/ddutartr/Projet/MONC/DATA/Fluorescence/HEp-2/'
#img_size = (1024,1536)
#img_size = (256,256)
def prepro_fluo(data_dir , img_size , batch_size):
    

    mu = (0.5)
    sigma = (0.5)
    transform = transforms.Compose([transforms.ToTensor(),
                                transforms.Resize(img_size),
                                transforms.Grayscale(num_output_channels=1),
                                transforms.Normalize(mu, sigma)])
                                
    dataset = []
    dataset= dset.ImageFolder(root=data_dir,
                          transform=transform)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                         shuffle=True, num_workers=0)
    return dataloader


def prepro_inference(img_path , img_size):
    subpath = './data_inf/'
    path_data = subpath + 'Fluo/'
    os.makedirs(path_data, exist_ok=True)
    img_name = img_path.split('/')
    shutil.copyfile(img_path, path_data + img_name[-1])
    dataloader = prepro_fluo(subpath , img_size , 1)
    return dataloader

def patch_decomposition(data_iteration, size = 512 , stride = 512 ):
    patches = data_iteration.unfold(2 , size , stride).unfold(3 , size, stride)
    return patches
    
def patch_feed(patches , batch_size = 8 , size = 512 , stride = 512):
    return patches.reshape(batch_size*patches.shape[2]*patches.shape[3], 1, size, stride)
    
def patch_recomposition(patches_out , img_size , batch_size , size , stride):
    patches_out = patches_out.reshape(batch_size , 1 , img_size[0]//size , img_size[1]//stride , size  , stride)
    patches_out = patches_out.reshape(batch_size, 1, img_size[0]//size, img_size[1]//stride, size*stride)
    patches_out = patches_out.permute(0, 1, 4, 2, 3).squeeze(1)
    patches_out = patches_out.view(batch_size, size*stride,-1)
    folded = F.fold(patches_out, img_size, kernel_size=size, stride=stride)
    return folded
    

